package prime.prime.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static prime.prime.web.TestUtils.toJson;

import com.icegreen.greenmail.util.ServerSetupTest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import prime.prime.domain.account.entity.Account;
import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.eventrequest.entity.EventRequest;
import prime.prime.domain.eventrequest.entity.EventRequestStatus;
import prime.prime.domain.eventrequest.models.EventRequestCreateDto;
import prime.prime.domain.eventrequest.models.EventRequestStatusDto;
import prime.prime.domain.eventrequest.models.EventRequestUpdateDto;
import prime.prime.domain.eventrequest.repository.EventRequestRepository;
import prime.prime.domain.role.Role;
import prime.prime.domain.season.entity.Season;
import prime.prime.domain.season.entity.SeasonDurationType;
import prime.prime.domain.season.repository.SeasonRepository;
import prime.prime.domain.technology.entity.Technology;
import prime.prime.domain.user.entity.User;
import prime.prime.domain.user.repository.UserRepository;
import prime.prime.infrastructure.exception.EventRequestException;
import prime.prime.infrastructure.exception.NotFoundException;
import prime.prime.infrastructure.security.ProgresoUserDetails;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;

@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
@Transactional
@TestPropertySource("classpath:application-test.properties")
class EventRequestControllerIntegrationTest {

    private static final String URL_PREFIX = "/events/request";
    private static final String USERNAME_ADMIN = "admin@xprmnt.xyz";
    private static final String USERNAME_MENTOR = "mentor@example.com";
    private static final String USERNAME_INTERN = "intern@example.com";

    private static EventRequestCreateDto eventRequestCreateDto;
    private static EventRequestCreateDto eventRequestCreateDtoSecond;
    private static ProgresoUserDetails adminUserDetails;
    private static ProgresoUserDetails internUserDetails;
    private static ProgresoUserDetails mentorUserDetails;
    private static EventRequest eventRequest;
    private static Long eventRequestId;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EventRequestRepository eventRequestRepository;
    @Autowired
    private SeasonRepository seasonRepository;
    @Autowired
    private UserRepository userRepository;

    @Container
    private static final MySQLContainer container = new MySQLContainer("mysql:8.0.32")
        .withDatabaseName("progreso");

    @RegisterExtension
    static GreenMailExtension greenMail = new GreenMailExtension(ServerSetupTest.SMTP)
        .withConfiguration(GreenMailConfiguration.aConfig().withUser("progreso", "password"))
        .withPerMethodLifecycle(false);

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        TestUtils.configureDBContainer(registry, container);
    }

    @BeforeEach
    void setUp() {
        eventRequest = new EventRequest();
        eventRequest.setStatus(EventRequestStatus.REQUESTED);
        eventRequest.setTitle("Meeting");
        eventRequest.setId(1L);
        eventRequest.setDescription("Daily meeting");

        Technology technology = new Technology(1L, "Java");

        Set<Technology> technologies = new HashSet<>();
        technologies.add(technology);

        Season season = new Season();
        season.setId(1L);
        season.setName("Season 1");
        season.setDurationType(SeasonDurationType.MONTHS);
        season.setDurationValue(6);
        season.setStartDate(LocalDate.now());
        season.setEndDate(LocalDate.now().plusMonths(2));
        season.setTechnologies(technologies);

        Long seasonId = seasonRepository.save(season).getId();
        season.setId(seasonId);

        Set<Season> seasons = new HashSet<>();
        seasons.add(season);

        Account adminAccount = new Account();
        adminAccount.setId(1L);
        adminAccount.setEmail(USERNAME_ADMIN);
        adminAccount.setPassword("$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS");
        adminAccount.setRole(Role.ADMIN);
        adminAccount.setStatus(AccountStatus.ACTIVE);

        User adminUser = new User();
        adminUser.setId(1L);
        adminUser.setFullName("Testing");
        adminUser.setDateOfBirth(LocalDate.parse("2022-09-09"));
        adminUser.setLocation("Location");
        adminUser.setPhoneNumber("123456789");
        adminUser.setAccount(adminAccount);
        adminUser.setTechnologies(technologies);
        adminUser.setSeasons(seasons);

        Long adminUserId = userRepository.save(adminUser).getId();
        adminUser.setId(adminUserId);

        Account internAccount = new Account();
        internAccount.setId(2L);
        internAccount.setEmail(USERNAME_INTERN);
        internAccount.setPassword("$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS");
        internAccount.setRole(Role.INTERN);
        internAccount.setStatus(AccountStatus.ACTIVE);

        User internUser = new User();
        internUser.setId(2L);
        internUser.setFullName("Testing intern user");
        internUser.setDateOfBirth(LocalDate.parse("2022-09-09"));
        internUser.setLocation("Location");
        internUser.setPhoneNumber("123456789");
        internUser.setAccount(internAccount);
        internUser.setTechnologies(technologies);
        internUser.setSeasons(seasons);

        Long internUserId = userRepository.save(internUser).getId();
        internUser.setId(internUserId);

        Account mentorAccount = new Account();
        mentorAccount.setId(3L);
        mentorAccount.setEmail(USERNAME_MENTOR);
        mentorAccount.setPassword("$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS");
        mentorAccount.setRole(Role.MENTOR);
        mentorAccount.setStatus(AccountStatus.ACTIVE);

        User mentorUser = new User();
        mentorUser.setId(3L);
        mentorUser.setFullName("Testing mentor user");
        mentorUser.setDateOfBirth(LocalDate.parse("2022-09-09"));
        mentorUser.setLocation("Location");
        mentorUser.setPhoneNumber("123456789");
        mentorUser.setAccount(mentorAccount);
        mentorUser.setTechnologies(technologies);
        mentorUser.setSeasons(seasons);

        Long mentorUserId = userRepository.save(mentorUser).getId();
        mentorUser.setId(mentorUserId);

        Set<User> users = new HashSet<>();
        users.add(mentorUser);
        users.add(internUser);

        season.setUsers(users);

        eventRequest.setRequester(internUser);
        eventRequest.setAssignee(mentorUser);
        eventRequest.setSeason(season);

        eventRequestId = eventRequestRepository.save(eventRequest).getId();

        eventRequestCreateDto = new EventRequestCreateDto(
            "Meeting",
            "Daily meeting",
            seasonId
        );
        eventRequestCreateDtoSecond = new EventRequestCreateDto(
            "Q&A",
            "Ask anything session",
            seasonId
        );
        List<GrantedAuthority> adminAuthorities = new ArrayList<>();
        adminAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        List<GrantedAuthority> internAuthorities = new ArrayList<>();
        internAuthorities.add(new SimpleGrantedAuthority("ROLE_INTERN"));
        List<GrantedAuthority> mentorAuthorities = new ArrayList<>();
        mentorAuthorities.add(new SimpleGrantedAuthority("ROLE_MENTOR"));
        adminUserDetails = new ProgresoUserDetails(1L, adminUserId,
            "admin@progreso.com",
            "$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS",
            adminAuthorities);
        internUserDetails = new ProgresoUserDetails(2L, internUserId, USERNAME_INTERN,
            "$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS",
            internAuthorities);
        mentorUserDetails = new ProgresoUserDetails(3L, mentorUserId, USERNAME_MENTOR,
            "$2a$10$rnyVHMYp60f64RE1fwe7BO2fcUeZj2qsMG4SHyzqA0cBZtvAPXOAS",
            mentorAuthorities);

    }

    @Test
    void create_ValidEventRequestDto_LoggedWithAdmin_Created() throws Exception {
        mockMvc.perform(post(URL_PREFIX).with(user(adminUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"));
    }

    @Test
    void getPage_LoggedWithAdmin_Ok() throws Exception {
        mockMvc.perform(get(URL_PREFIX).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(1));
    }

    @Test
    void getPage_LoggedWithIntern_Ok() throws Exception {
        mockMvc.perform(get(URL_PREFIX).with(user(internUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(1));
    }

    @Test
    void getPage_LoggedWithMentor_Ok() throws Exception {
        mockMvc.perform(get(URL_PREFIX).with(user(mentorUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(1));
    }

    @Test
    void getById_ValidId_LoggedWithAdmin_Ok() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/" + eventRequestId).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"));
    }

    @Test
    void getById_ValidId_LoggedWithIntern_Ok() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/" + eventRequestId).with(user(internUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"));
    }

    @Test
    void getById_InvalidId_NotFound() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/100").with(user(adminUserDetails)))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException))
            .andExpect(result -> assertEquals("EventRequest with id : 100 not found;",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    void update_ValidDto_LoggedWithAdmin_Ok() throws Exception {
        EventRequestUpdateDto updateDto = new EventRequestUpdateDto("New title", "New description");
        mockMvc.perform(patch(URL_PREFIX + "/" + eventRequestId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateDto)).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("New title"))
            .andExpect(jsonPath("$.description").value("New description"));
    }

    @Test
    void update_ValidDto_LoggedWithIntern_Ok() throws Exception {
        EventRequestUpdateDto updateDto = new EventRequestUpdateDto("New title", "New description");
        mockMvc.perform(patch(URL_PREFIX + "/" + eventRequestId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateDto)).with(user(internUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("New title"))
            .andExpect(jsonPath("$.description").value("New description"));
    }

    @Test
    void update_InvalidId_NotFound() throws Exception {
        EventRequestUpdateDto updateDto = new EventRequestUpdateDto("New title", "New description");

        mockMvc.perform(patch(URL_PREFIX + "/10")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateDto)).with(user(adminUserDetails)))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException))
            .andExpect(result -> assertEquals("EventRequest with id : 10 not found;",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    void delete_ValidId_LoggedWithAdmin_Ok() throws Exception {
        mockMvc.perform(delete(URL_PREFIX + "/" + eventRequestId).with(user(adminUserDetails)))
            .andExpect(status().isNoContent());
    }

    @Test
    void delete_ValidId_LoggedWithIntern_Ok() throws Exception {
        mockMvc.perform(delete(URL_PREFIX + "/" + eventRequestId).with(user(internUserDetails)))
            .andExpect(status().isNoContent());
    }

    @Test
    void delete_InvalidId_NotFound() throws Exception {
        mockMvc.perform(delete(URL_PREFIX + "/10").with(user(adminUserDetails)))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException))
            .andExpect(result -> assertEquals("EventRequest with id : 10 not found;",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    void create_ValidEventRequestDtoSecond_Created() throws Exception {
        mockMvc.perform(post(URL_PREFIX).with(user(adminUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDtoSecond)))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Q&A"))
            .andExpect(jsonPath("$.description").value("Ask anything session"));
    }

    @Test
    void changeStatus_Reject_LoggedWithAdmin_Ok() throws Exception {
        eventRequest.setStatus(EventRequestStatus.REQUESTED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto eventRequestStatusDto = new EventRequestStatusDto("Not needed",
            EventRequestStatus.REJECTED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestStatusDto)).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("REJECTED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request rejected: Meeting", message.getSubject());
    }

    @Test
    void changeStatus_Reject_LoggedWithMentor_Ok() throws Exception {
        eventRequest.setStatus(EventRequestStatus.REQUESTED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto eventRequestStatusDto = new EventRequestStatusDto("Not needed",
            EventRequestStatus.REJECTED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestStatusDto)).with(user(mentorUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("REJECTED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request rejected: Meeting", message.getSubject());
    }

    @Test
    void changeStatus_RejectAlreadyRejected_Conflict() throws Exception {
        eventRequest.setStatus(EventRequestStatus.REJECTED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        EventRequestStatusDto eventRequestStatusDto = new EventRequestStatusDto("Not needed",
            EventRequestStatus.REJECTED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestStatusDto)).with(user(adminUserDetails)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof EventRequestException));
    }

    @Test
    void changeStatus_Approve_LoggedWithAdmin_Ok() throws Exception {
        eventRequest.setStatus(EventRequestStatus.REQUESTED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto approveDto = new EventRequestStatusDto("Additional information...",
            EventRequestStatus.APPROVED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(approveDto)).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("APPROVED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request approved: Meeting", message.getSubject());
    }

    @Test
    void changeStatus_Approve_LoggedWithMentor_Ok() throws Exception {
        eventRequest.setStatus(EventRequestStatus.REQUESTED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto approveDto = new EventRequestStatusDto("Additional information...",
            EventRequestStatus.APPROVED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(approveDto)).with(user(mentorUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("APPROVED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request approved: Meeting", message.getSubject());
    }

    @Test
    void changeStatus_ApproveAlreadyApproved_Conflict() throws Exception {
        eventRequest.setStatus(EventRequestStatus.APPROVED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        EventRequestStatusDto approveDto = new EventRequestStatusDto("Additional information...",
            EventRequestStatus.APPROVED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(approveDto)).with(user(adminUserDetails)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof EventRequestException));
    }

    @Test
    void changeStatus_Schedule_LoggedWithAdmin_OK() throws Exception {
        eventRequest.setStatus(EventRequestStatus.APPROVED);

        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto scheduleDTO = new EventRequestStatusDto("Scheduled meeting",
            EventRequestStatus.SCHEDULED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(scheduleDTO)).with(user(adminUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("SCHEDULED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request scheduled: Meeting", message.getSubject());
    }

    @Test
    void changeStatus_Schedule_LoggedWithMentor_OK() throws Exception {
        eventRequest.setStatus(EventRequestStatus.APPROVED);

        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        greenMail.purgeEmailFromAllMailboxes();

        EventRequestStatusDto scheduleDTO = new EventRequestStatusDto("Scheduled meeting",
            EventRequestStatus.SCHEDULED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(scheduleDTO)).with(user(mentorUserDetails)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title").value("Meeting"))
            .andExpect(jsonPath("$.description").value("Daily meeting"))
            .andExpect(jsonPath("$.status").value("SCHEDULED"));

        MimeMessage message = greenMail.getReceivedMessages()[0];

        assertEquals(1, message.getAllRecipients().length);
        assertEquals("Event request scheduled: Meeting", message.getSubject());
    }


    @Test
    void changeStatus_ScheduleAlreadyScheduled_Conflict() throws Exception {
        eventRequest.setStatus(EventRequestStatus.SCHEDULED);
        EventRequest savedEventRequest = eventRequestRepository.save(eventRequest);

        EventRequestStatusDto scheduleDTO = new EventRequestStatusDto("Scheduled meeting",
            EventRequestStatus.SCHEDULED, null);

        mockMvc.perform(post(URL_PREFIX + "/" + savedEventRequest.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(scheduleDTO)).with(user(adminUserDetails)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof EventRequestException));
    }

    @Test
    void changeStatus_InvalidId_NotFound() throws Exception {
        EventRequestStatusDto scheduleDTO = new EventRequestStatusDto("Scheduled meeting",
            EventRequestStatus.SCHEDULED, null);

        mockMvc.perform(post(URL_PREFIX + "/20/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(scheduleDTO)).with(user(adminUserDetails)))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException));
    }

    @Test
    @WithAnonymousUser
    void create_NoLoggedUser_Unauthorized() throws Exception {
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    void create_ValidDtoMentorGiven_Forbidden() throws Exception {
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)).with(user(mentorUserDetails)))
            .andExpect(status().isForbidden());
    }

    @Test
    void update_ValidDtoMentorGiven_Forbidden() throws Exception {
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)).with(user(mentorUserDetails)))
            .andExpect(status().isForbidden());
    }

    @Test
    void delete_ValidDtoMentorGiven_Forbidden() throws Exception {
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)).with(user(mentorUserDetails)))
            .andExpect(status().isForbidden());
    }

    @Test
    void changeStatus_ValidDtoMentorGiven_Forbidden() throws Exception {
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(eventRequestCreateDto)).with(user(mentorUserDetails)))
            .andExpect(status().isForbidden());
    }
}