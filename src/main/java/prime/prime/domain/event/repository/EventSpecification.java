package prime.prime.domain.event.repository;

import java.lang.reflect.Field;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import prime.prime.domain.event.entity.Event;
import prime.prime.domain.event.models.SearchEventDto;

public class EventSpecification implements Specification<Event> {

    private final SearchEventDto searchEventDto;


    public EventSpecification(SearchEventDto searchEventDto) {
        this.searchEventDto = searchEventDto;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> query,
        CriteriaBuilder criteriaBuilder) {

        Predicate p = criteriaBuilder.conjunction();

        if (searchEventDto.seasonId() == null) {
            return p;
        }

        Field[] declaredFields = searchEventDto.getClass().getDeclaredFields();

        for (Field searchField : declaredFields) {

            Predicate predicate;
            if (searchField.getName().equals("seasonId")) {
                predicate = criteriaBuilder.equal(
                    root.get("season").get("id"),
                    searchEventDto.seasonId());
            } else {
                predicate = criteriaBuilder.conjunction();
            }

            p.getExpressions().add(criteriaBuilder.and(predicate));
        }
        return p;

    }
}
