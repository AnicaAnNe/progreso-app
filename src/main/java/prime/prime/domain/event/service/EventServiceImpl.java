package prime.prime.domain.event.service;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import prime.prime.domain.calendar.service.CalendarServiceImpl;
import prime.prime.domain.event.entity.Event;
import prime.prime.domain.event.mappers.EventMapper;
import prime.prime.domain.event.models.EventRequestDto;
import prime.prime.domain.event.models.EventResponseWithAttendeesDto;
import prime.prime.domain.event.models.EventUpdateDto;
import prime.prime.domain.event.models.SearchEventDto;
import prime.prime.domain.event.repository.EventRepository;
import prime.prime.domain.event.repository.EventSpecification;
import prime.prime.domain.eventattendees.entity.EventAttendee;
import prime.prime.domain.season.service.SeasonService;
import prime.prime.domain.user.entity.IntegrationType;
import prime.prime.domain.user.entity.User;
import prime.prime.domain.user.service.UserService;
import prime.prime.infrastructure.email_sender.config.Email;
import prime.prime.infrastructure.email_sender.service.EmailService;
import prime.prime.infrastructure.exception.EventException;
import prime.prime.infrastructure.exception.MultipleAttendeesException;
import prime.prime.infrastructure.exception.NotFoundException;
import prime.prime.infrastructure.exception.SeasonException;
import prime.prime.infrastructure.security.ProgresoUserDetails;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final EventMapper eventMapper;
    private final UserService userService;
    private final EmailService emailService;
    private final CalendarServiceImpl calendarServiceImpl;

    private final SeasonService seasonService;

    public EventServiceImpl(EventRepository eventRepository,
        EventMapper eventMapper,
        UserService userService,
        EmailService emailService,
        CalendarServiceImpl calendarServiceImpl, SeasonService seasonService) {
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
        this.userService = userService;
        this.emailService = emailService;
        this.calendarServiceImpl = calendarServiceImpl;
        this.seasonService = seasonService;
    }

    private void addEventAttendees(Set<Long> newAttendees, Event event, boolean required) {
        for (Long attendeeId : newAttendees) {
            event.getEventAttendees()
                .add(new EventAttendee(required, event, userService.getEntityById(attendeeId)));
        }
    }

    @Override
    public EventResponseWithAttendeesDto create(EventRequestDto eventRequestDto,
        ProgresoUserDetails userDetails) {
        User creator = userService.getEntityById(userDetails.getUserId());

        var optionalAttendees = eventRequestDto.optionalAttendees();
        var requiredAttendees = eventRequestDto.requiredAttendees();

        checkForMultipleAttendees(optionalAttendees, requiredAttendees);

        Event saveEvent = eventMapper.dtoToEvent(eventRequestDto);
        saveEvent.setCreator(creator);
        saveEvent.setEventAttendees(new HashSet<>());
        saveEvent.setSeason(seasonService.findActiveSeason(creator, eventRequestDto.seasonId()));

        addEventAttendees(optionalAttendees, saveEvent, false);
        addEventAttendees(requiredAttendees, saveEvent, true);

        isCreatorPresent(saveEvent);

        eventRepository.save(saveEvent);
        if (hasAttendeesWithIntegration(saveEvent.getEventAttendees(), IntegrationType.GOOGLE)) {
            saveGoogleEventIdOfCreatedEvent(saveEvent);
        }
        sendEmailToAttendees(saveEvent, creator, true);

        return eventMapper.eventToEventWithAttendeesDto(saveEvent);
    }

    private void saveGoogleEventIdOfCreatedEvent(Event createdEvent) {
        createdEvent.setGoogleCalendarEventId(calendarServiceImpl.createEvent(createdEvent));
        eventRepository.save(createdEvent);
    }

    @Override
    public EventResponseWithAttendeesDto getById(Long id) {
        return eventMapper.eventToEventWithAttendeesDto(eventRepository.findById(id)
            .orElseThrow(
                () -> new NotFoundException(Event.class.getSimpleName(), "id", id.toString())
            )
        );
    }

    @Override
    public Page<EventResponseWithAttendeesDto> getAll(SearchEventDto searchEventDto,
        Pageable pageable, ProgresoUserDetails progresoUserDetails) {
        if (searchEventDto.seasonId() == null && !progresoUserDetails.hasRole("ROLE_ADMIN")) {
            throw new EventException("Season id cannot be null");
        }

        User user = userService.getEntityById(progresoUserDetails.getUserId());
        if (user.isAdmin() && searchEventDto.seasonId() != null) {
            seasonService.existsById(searchEventDto.seasonId());
        }

        if (user.getSeasons() == null || user.getSeasons().stream()
            .noneMatch(season -> season.getId().equals(searchEventDto.seasonId()))
            && !user.isAdmin()) {
            throw new SeasonException(user.getId(), searchEventDto.seasonId());
        }

        return eventRepository
            .findAll(new EventSpecification(searchEventDto), pageable)
            .map(eventMapper::eventToEventWithAttendeesDto);
    }

    private void checkForMultipleAttendees(Set<Long> attendees, Set<Long> requiredAttendees) {
        boolean check = attendees.stream()
            .anyMatch(requiredAttendee -> requiredAttendees.stream()
                .anyMatch(a -> a.equals(requiredAttendee))
            );

        if (check) {
            throw new MultipleAttendeesException("Can not pass the same attendee in both lists!");
        }
    }

    @Override
    public EventResponseWithAttendeesDto update(Long id, EventUpdateDto eventUpdateDto,
        ProgresoUserDetails userDetails) {
        User editor = userService.getEntityById(userDetails.getUserId());

        Event foundEvent = eventRepository.findById(id)
            .orElseThrow(
                () -> new NotFoundException(Event.class.getSimpleName(), "id", id.toString())
            );

        if (!CollectionUtils.isEmpty(eventUpdateDto.optionalAttendees())
            && !CollectionUtils.isEmpty(eventUpdateDto.requiredAttendees())) {
            checkForMultipleAttendees(eventUpdateDto.optionalAttendees(),
                eventUpdateDto.requiredAttendees());
        }

        updateEvent(eventUpdateDto, foundEvent);

        eventRepository.save(foundEvent);

        updateGoogleEventOfUpdatedEvent(foundEvent);

        if (eventUpdateDto.notifyAttendees()) {
            sendEmailToAttendees(foundEvent, editor, false);
        }

        return eventMapper.eventToEventWithAttendeesDto(foundEvent);
    }

    @Override
    public void delete(Long id) {
        Event foundEvent = eventRepository.findById(id)
            .orElseThrow(
                () -> new NotFoundException(Event.class.getSimpleName(), "id", id.toString())
            );
        boolean hasGoogleCalenderEventId = hasGoogleCalenderEvent(foundEvent);
        eventRepository.delete(foundEvent);
        if (hasGoogleCalenderEventId) {
            calendarServiceImpl.deleteEvent(foundEvent);
        }
    }

    private void updateEvent(EventUpdateDto eventUpdateDto, Event foundEvent) {

        var allAttendees = new HashSet<EventAttendee>();

        if (eventUpdateDto.optionalAttendees() != null) {
            eventUpdateDto.optionalAttendees().forEach(userId -> allAttendees.add(
                new EventAttendee(false, foundEvent, userService.getEntityById(userId))));
        }

        if (eventUpdateDto.requiredAttendees() != null) {
            eventUpdateDto.requiredAttendees().forEach(userId -> allAttendees.add(
                new EventAttendee(true, foundEvent, userService.getEntityById(userId))));
        }

        foundEvent.setEventAttendees(allAttendees);
        isCreatorPresent(foundEvent);
        eventMapper.updateEventFromDto(eventUpdateDto, foundEvent);
    }

    private void isCreatorPresent(Event foundEvent) {
        var isCreatorPresent = foundEvent.getEventAttendees().stream().anyMatch(
            eventAttendee -> eventAttendee.getUser().getId()
                .equals(foundEvent.getCreator().getId()));

        if (!isCreatorPresent) {
            foundEvent.getEventAttendees()
                .add(new EventAttendee(true, foundEvent, foundEvent.getCreator()));
        }
    }

    private void sendEmailToAttendees(Event event, User loggedUser, boolean isNewEvent) {
        String eventAction = isNewEvent ? "created" : "updated";
        String subject = "Event " + eventAction + ": " + event.getTitle();
        Map<String, String> templateData = new HashMap<>();

        for (EventAttendee attendee : event.getEventAttendees()) {
            if (attendee.getUser().getId().equals(loggedUser.getId())) {
                continue;
            }
            sendEmailNotification(attendee.getUser(), event, subject, eventAction, templateData);
        }
    }

    private void sendEmailNotification(User user, Event event, String emailSubject,
        String eventAction, Map<String, String> templateData) {
        templateData.put("fullName", user.getFullName());
        templateData.put("title", event.getTitle());
        templateData.put("description", event.getDescription());
        templateData.put("startTime",
            event.getStartTime().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")));
        templateData.put("duration", event.getDuration().toMinutes() + " minutes");
        templateData.put("eventAction", eventAction);
        templateData.put("template", "event_notification");

        Email email = new Email(user.getAccount().getEmail(), emailSubject, templateData);
        emailService.send(email);
    }

    private boolean hasAttendeesWithIntegration(Set<EventAttendee> eventAttendees,
        IntegrationType integrationType) {
        return eventAttendees.stream()
            .anyMatch(eventAttendee -> eventAttendee.getUser().hasIntegration(integrationType));
    }

    private boolean hasGoogleCalenderEvent(Event event) {
        return event.getGoogleCalendarEventId() != null;
    }

    private void updateGoogleEventOfUpdatedEvent(Event event) {
        if (!hasGoogleCalenderEvent(event) && hasAttendeesWithIntegration(
            event.getEventAttendees(), IntegrationType.GOOGLE)) {
            saveGoogleEventIdOfCreatedEvent(event);
        }

        if (hasAttendeesWithIntegration(event.getEventAttendees(),
            IntegrationType.GOOGLE)) {
            calendarServiceImpl.updateEvent(event);
        }

        if (hasGoogleCalenderEvent(event) && !hasAttendeesWithIntegration(
            event.getEventAttendees(), IntegrationType.GOOGLE)) {
            calendarServiceImpl.deleteEvent(event);
            event.setGoogleCalendarEventId(null);
            eventRepository.save(event);
        }
    }
}