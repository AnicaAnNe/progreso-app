package prime.prime.domain.event.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import prime.prime.domain.event.models.EventRequestDto;
import prime.prime.domain.event.models.EventResponseWithAttendeesDto;
import prime.prime.domain.event.models.EventUpdateDto;
import prime.prime.domain.event.models.SearchEventDto;
import prime.prime.infrastructure.security.ProgresoUserDetails;

public interface EventService {

    EventResponseWithAttendeesDto create(EventRequestDto eventRequestDto,
        ProgresoUserDetails userDetails);

    EventResponseWithAttendeesDto getById(Long id);

    Page<EventResponseWithAttendeesDto> getAll(SearchEventDto searchEventDto, Pageable pageable,
        ProgresoUserDetails progresoUserDetails);

    EventResponseWithAttendeesDto update(Long id, EventUpdateDto eventUpdateDto,
        ProgresoUserDetails progresoUserDetails);

    void delete(Long id);
}
