package prime.prime.domain.event.mappers;

import org.mapstruct.*;
import prime.prime.domain.event.entity.Event;
import prime.prime.domain.event.mappers.custom.EventDurationMapper;
import prime.prime.domain.event.mappers.custom.EventDurationMapping;
import prime.prime.domain.event.models.EventRequestDto;
import prime.prime.domain.event.models.EventResponseDto;
import prime.prime.domain.event.models.EventResponseWithAttendeesDto;
import prime.prime.domain.event.models.EventUpdateDto;
import prime.prime.domain.season.service.SeasonService;
import prime.prime.domain.user.mappers.UserMapper;
import prime.prime.domain.user.mappers.custom.UserIdToUserEntityMapper;
import prime.prime.domain.user.mappers.custom.UserMapping;
import prime.prime.domain.user.service.UserService;


@Mapper(
        componentModel = "spring",
        uses = {UserService.class,
                UserMapper.class,
                EventDurationMapper.class,
                UserIdToUserEntityMapper.class,
                SeasonService.class
        }
)
public interface EventMapper {

    @Mapping(target = "duration", qualifiedBy = EventDurationMapping.class)
    @Mapping(source = "creator.id", target = "creatorId")
    EventResponseDto eventToDto(Event event);

    @Mapping(target = "duration", qualifiedBy = EventDurationMapping.class)
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "eventAttendees", target = "attendees", qualifiedBy = UserMapping.class)
    @Mapping(source = "season.id", target = "seasonId")
    EventResponseWithAttendeesDto eventToEventWithAttendeesDto(Event event);

    @Mapping(target = "duration", qualifiedBy = EventDurationMapping.class)
    Event dtoToEvent(EventRequestDto eventRequestDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "duration", qualifiedBy = EventDurationMapping.class)
    @Mapping(target = "eventAttendees", ignore = true)
    void updateEventFromDto(EventUpdateDto dto, @MappingTarget Event event);
}
