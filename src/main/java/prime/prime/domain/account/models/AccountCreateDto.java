package prime.prime.domain.account.models;

import prime.prime.domain.role.Role;
import prime.prime.infrastructure.validation.EnumValidation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record AccountCreateDto(
        @NotBlank(message = "Email is mandatory")
        @Email(message = "Email should be valid")
        String email,

        @NotNull(message = "Role is mandatory")
        @EnumValidation(value = Role.class)
        String role
        ) {
}
