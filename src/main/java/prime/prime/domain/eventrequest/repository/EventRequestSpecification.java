package prime.prime.domain.eventrequest.repository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import prime.prime.domain.eventrequest.entity.EventRequest;
import prime.prime.domain.eventrequest.entity.EventRequestStatus;
import prime.prime.domain.eventrequest.models.SearchEventRequestDto;

public class EventRequestSpecification implements Specification<EventRequest> {

    private final SearchEventRequestDto searchEventRequestDto;
    private final Set<Long> seasons;

    public EventRequestSpecification(SearchEventRequestDto searchEventRequestDto,
        Set<Long> seasons) {
        this.searchEventRequestDto = searchEventRequestDto;
        this.seasons = seasons;
    }

    @Override
    public Predicate toPredicate(Root<EventRequest> root, CriteriaQuery<?> query,
        CriteriaBuilder criteriaBuilder) {

        Predicate p = criteriaBuilder.conjunction();

        List<Predicate> predicates = new ArrayList<>();
        if (!seasons.isEmpty()) {
            seasons.forEach(season -> {
                Predicate predicate = criteriaBuilder.equal(root.get("season").get("id"), season);
                predicates.add(predicate);

            });
            Predicate seasonPredicate = criteriaBuilder.or(predicates.toArray(new Predicate[] {}));
            p.getExpressions().add(seasonPredicate);
        }

        Field[] declaredFields = searchEventRequestDto.getClass().getDeclaredFields();

        for (Field searchField : declaredFields) {
            searchField.setAccessible(true);

            String searchValue;
            try {
                searchValue = (String) searchField.get(searchEventRequestDto);

                if (searchValue == null || (searchField.getName().equals("status")
                    && !isStatusValid(searchValue))) {
                    continue;
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            Predicate predicate;
            if (searchField.getName().equals("status")) {
                predicate = criteriaBuilder.equal(
                    root.get("status"),
                    EventRequestStatus.valueOf(searchValue.toUpperCase(Locale.ROOT)));
            } else if (searchField.getName().equals("seasonId")) {
                predicate = criteriaBuilder.equal(
                    root.get("season").get("id"), searchEventRequestDto.seasonId());
            } else {
                predicate = criteriaBuilder.conjunction();
            }

            p.getExpressions().add(criteriaBuilder.and(predicate));
        }
        return p;
    }

    private boolean isStatusValid(String givenStatus) {
        for (EventRequestStatus status : EventRequestStatus.values()) {
            if (status.name().equals(givenStatus.toUpperCase())) {
                return true;
            }
        }

        return false;
    }
}