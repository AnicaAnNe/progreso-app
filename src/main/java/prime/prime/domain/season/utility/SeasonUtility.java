package prime.prime.domain.season.utility;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;
import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.role.Role;
import prime.prime.domain.season.entity.Season;
import prime.prime.domain.user.entity.User;
import prime.prime.infrastructure.exception.InvalidDateException;
import prime.prime.infrastructure.exception.SeasonException;

public class SeasonUtility {

    private static final int MAX_SEASON_COUNT_PER_INTERN = 3;

    public static void validateStartDateIsBeforeEndDate(LocalDate startDate, LocalDate endDate) {
        if (!startDate.isBefore(endDate)) {
            throw new InvalidDateException("Start date cannot be after or the same as end date");
        }
    }

    public static void validateUserRoles(Set<User> users, Role role) {
        var invalidUsers = users
            .stream()
            .filter(user -> !user.getAccount().getRole().equals(role))
            .map(User::getId)
            .toList();

        if (!invalidUsers.isEmpty()) {
            throw new SeasonException(User.class.getSimpleName(), "id", invalidUsers.toString(),
                role);
        }
    }

    public static void validateUserStatus(Set<User> users) {
        var invalidUsers = users
            .stream()
            .filter(user -> !user.getAccount().getStatus().equals(AccountStatus.ACTIVE))
            .map(User::getId)
            .toList();

        if (!invalidUsers.isEmpty()) {
            throw new SeasonException(User.class.getSimpleName(), "id", invalidUsers.toString());
        }
    }

    public static void validateInterns(Season season, Set<User> interns) {
        var invalidInterns = interns
            .stream()
            .filter(intern -> (intern.getSeasons().size() >= MAX_SEASON_COUNT_PER_INTERN))
            .map(User::getId)
            .toList();

        if (season.getUsers() != null) {
            var userIds = season.getUsers()
                .stream()
                .map(User::getId)
                .collect(Collectors.toSet());

            invalidInterns = invalidInterns
                .stream()
                .filter(id -> !userIds.contains(id))
                .toList();
        }

        if (!invalidInterns.isEmpty()) {
            throw new SeasonException("Interns", "id", invalidInterns.toString(),
                MAX_SEASON_COUNT_PER_INTERN);
        }
    }

    public static Set<User> filterAssigneesByRole(Season season, Role role) {
        if (season.getUsers() == null) {
            return null;
        }

        return season.getUsers().stream()
            .filter(user -> user.getAccount().getRole().equals(role))
            .collect(Collectors.toSet());
    }

    public static Set<User> getInterns(Season season) {
        return season.getUsers().stream()
            .filter(user -> user.getAccount().getRole().equals(Role.INTERN))
            .collect(Collectors.toSet());
    }

    public static Set<User> getMentors(Season season) {
        return season.getUsers().stream()
            .filter(user -> user.getAccount().getRole().equals(Role.MENTOR))
            .collect(Collectors.toSet());
    }
}
