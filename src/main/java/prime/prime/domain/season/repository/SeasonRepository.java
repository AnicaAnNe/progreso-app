package prime.prime.domain.season.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import prime.prime.domain.season.entity.Season;

@Repository
public interface SeasonRepository extends JpaRepository<Season, Long> {
  @Query("SELECT DISTINCT season FROM Season season LEFT JOIN FETCH season.users users WHERE season.id = :id")
  Optional<Season> findByIdAndFetchUsers(Long id);

  @Query("SELECT season FROM Season season LEFT JOIN FETCH season.users users")
  List<Season> findAllAndFetchUsers(Pageable pageable);

  boolean existsById(Long id);
}
