package prime.prime.domain.technology.models;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

public record TechnologyCreateDto(
        @NotBlank(message = "Name is mandatory")
        @Column(unique = true)
        String name
){
}
