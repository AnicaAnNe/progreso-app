package prime.prime.infrastructure.exception;

public class ChangePasswordAccessException extends RuntimeException{
    public ChangePasswordAccessException() {super("User can change its own password only");}
}
