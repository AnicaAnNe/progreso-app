package prime.prime.infrastructure.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @Value("${spring.servlet.multipart.max-file-size}")
    private String MAX_IMAGE_SIZE;

    @ExceptionHandler({NotFoundException.class, ArchivedUserException.class})
    public ResponseEntity<ErrorResponse> handleNotFoundException(RuntimeException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler({InvalidOldPasswordException.class, DuplicateException.class,
        ChangePasswordAccessException.class, EventRequestException.class, EventException.class})
    public ResponseEntity<ErrorResponse> handleConflictExceptions(RuntimeException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.CONFLICT)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler(EmailSendException.class)
    public ResponseEntity<ErrorResponse> handleEmailSendException(EmailSendException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleInvalidArgument(ConstraintViolationException e) {
        List<Violation> violations = new ArrayList<>();
        for (ConstraintViolation<?> violation : e.getConstraintViolations()
        ) {
            violations.add(new Violation(
                violation.getPropertyPath().toString(),
                e.getMessage(),
                LocalDateTime.now()));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(violations));
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public ResponseEntity<ErrorResponse> handleInvalidCredentials(BindException e) {
        List<Violation> violations = new ArrayList<>();
        e.getBindingResult().getFieldErrors().forEach(error -> violations.add(
            new Violation(error.getField(), error.getDefaultMessage(),
                LocalDateTime.now())));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(violations));
    }

    @ExceptionHandler(JsonMappingException.class)
    public ResponseEntity<ErrorResponse> handleDateTimeParseExceptions(JsonMappingException e) {
        var references = e.getPath();

        var violation = new Violation(references.get(0).getFieldName(),
                getError(e.getMessage()),
                LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler({PropertyReferenceException.class, LastAdminException.class,
        InvalidDateException.class, NoTechnologyException.class,
        GoogleAuthorizationException.class, MultipleAttendeesException.class, SeasonException.class})
    public ResponseEntity<ErrorResponse> handleWrongPropertyValueAndLastAdminDeletion(
        RuntimeException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorResponse> handleAccessDenied(AccessDeniedException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.FORBIDDEN)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler(SizeLimitExceededException.class)
    public ResponseEntity<ErrorResponse> handleSizeLimitException() {
        Violation violation = new Violation(null, "Max image size is " + MAX_IMAGE_SIZE,
            LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED)
            .body(new ErrorResponse(List.of(violation)));
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleMethodNotAllowed(
        HttpRequestMethodNotSupportedException e) {
        Violation violation = new Violation(null, e.getMessage(), LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
            .body(new ErrorResponse(List.of(violation)));
    }

    private record ErrorResponse(List<?> violations) {

    }

    private record Violation(String field, String error, LocalDateTime timestamp) {

    }

    private String getError(String error) {
        if (error.contains("escape")) {
            return "Invalid character used";
        }
        return "Invalid format, please compare your request to application documentation";
    }
}
