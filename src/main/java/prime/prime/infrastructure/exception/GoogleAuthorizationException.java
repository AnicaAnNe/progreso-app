package prime.prime.infrastructure.exception;

public class GoogleAuthorizationException extends RuntimeException{
    public GoogleAuthorizationException(String message) {
        super(message);
    }

}
