package prime.prime.infrastructure.exception;

public class ArchivedUserException extends RuntimeException {

    public ArchivedUserException(String className, String fieldName, Long fieldValue) {
        super(className + " with " + fieldName + " : " + fieldValue + " is archived");
    }
}
