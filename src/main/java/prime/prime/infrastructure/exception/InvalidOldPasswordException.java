package prime.prime.infrastructure.exception;

public class InvalidOldPasswordException extends RuntimeException{
    public InvalidOldPasswordException() {
        super("Incorrect old password");
    }
}
