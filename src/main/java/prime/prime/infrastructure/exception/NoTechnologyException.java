package prime.prime.infrastructure.exception;

public class NoTechnologyException extends RuntimeException{
    public NoTechnologyException() {super("Mentor or intern should have at least one technology assigned to them");}
}
