package prime.prime.web;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import prime.prime.domain.account.models.AccountReturnWithPasswordDto;
import prime.prime.domain.account.service.AccountService;

@RestController
@RequestMapping("/account")
@Hidden
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{email}")
    ResponseEntity<AccountReturnWithPasswordDto> getAccountByEmail(@PathVariable String email) {
        return ResponseEntity.ok(accountService.returnAccountByEmail(email));
    }
}
